package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.JungleLove;
import com.mygdx.game.util.Constants;

/**
 * Created by sergio on 05/06/2016.
 */
public class ConfigurationScreen implements Screen {

    JungleLove game;
    Stage stage;
    Preferences prefs;

    public ConfigurationScreen(JungleLove game) {
        this.game = game;
    }

    private void loadScreen() {

        stage = new Stage();

        Table table = new Table(game.getSkin());
        table.setFillParent(true);
        table.center();

        Label title = new Label("JUNGLE LOVE\nCONFIGURACION", game.getSkin());
        title.setColor(Color.GREEN);
        title.setFontScale(1.5f);

        final CheckBox checkSound = new CheckBox(" SONIDO", game.getSkin());
        checkSound.setColor(Color.YELLOW);
        checkSound.setChecked(prefs.getBoolean("sound"));
        checkSound.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                prefs.putBoolean("sound", checkSound.isChecked());
            }
        });

        final CheckBox checkDificulty = new CheckBox(" DIFICULTAD EXTREMA", game.getSkin());
        checkDificulty.setColor(Color.YELLOW);
        checkDificulty.setChecked(prefs.getBoolean("dificulty"));
        checkDificulty.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                prefs.putBoolean("dificulty", checkDificulty.isChecked());
            }
        });

        TextButton exitButton = new TextButton("MENU PRINCIPAL", game.getSkin());
        exitButton.setColor(Color.GREEN);
        exitButton.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                prefs.flush();
                dispose();
                game.setScreen(new MainMenuScreen(game));
            }
        });

        Label aboutUs = new Label("JUNGLE LOVE", game.getSkin());
        aboutUs.setFontScale(2f);

        table.row().height(200);
        table.add(title).center().pad(35f);
        table.row().height(20);
        table.add(checkSound).center().pad(5f);
        table.row().height(20);
        table.add(checkDificulty).center().pad(5f);
        table.row().height(20);
        table.add(exitButton).center().width(300).pad(5f);
        table.row().height(70);
        table.add(aboutUs).center().pad(55f);

        stage.addActor(table);
        Gdx.input.setInputProcessor(stage);
    }

    private void loadPreferences() {
        prefs = Gdx.app.getPreferences(Constants.APP_NAME);
        if (!prefs.contains("sound"))
            prefs.putBoolean("sound", true);
        if(!prefs.contains("dificulty"))
            prefs.putBoolean("dificulty", true);
    }
    @Override
    public void show() {
        loadPreferences();
        loadScreen();
    }

    @Override
    public void render(float dt) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(dt);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
