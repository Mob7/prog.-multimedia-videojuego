package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.mygdx.game.JungleLove;
import com.mygdx.game.managers.CameraManager;
import com.mygdx.game.managers.LevelManager;
import com.mygdx.game.managers.RenderManager;
import com.mygdx.game.managers.SpriteManager;

/**
 * Created by sergio on 05/06/2016.
 */
public class GameScreen implements Screen {


    JungleLove game;
    SpriteManager spriteManager;
    LevelManager levelManager;
    CameraManager cameraManager;
    RenderManager renderManager;
    int level;
    int lives;
    int score;

    public GameScreen(JungleLove game, int level, int lives, int score) {
        this.game = game;
        this.level = level;
        this.lives = lives;
        this.score = score;

        spriteManager = new SpriteManager(game);
        levelManager = new LevelManager(spriteManager);
        if(level==2){
            levelManager.setLevel(2);
        }
        levelManager.loadCurrentLevel();

        spriteManager.setLevelManager(levelManager);
        cameraManager = new CameraManager(spriteManager, levelManager);
        levelManager.setCameraManager(cameraManager);
        spriteManager.setCameraManager(cameraManager);
        renderManager = new RenderManager(spriteManager, cameraManager, levelManager.batch, level, lives, score);
    }
    @Override
    public void show() {

    }

    @Override
    public void render(float dt) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        cameraManager.handleCamera();
        spriteManager.update(dt);
        renderManager.drawFrame();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
