package com.mygdx.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.mygdx.game.JungleLove;

/**
 * Created by sergio on 05/06/2016.
 */
public class GameOverScreen implements Screen{


    final JungleLove game;
    private Stage stage;

    public GameOverScreen(JungleLove game){
        this.game=game;
    }

    @Override
    public void show() {
        stage = new Stage();

        Table table = new Table(game.getSkin());
        table.setFillParent(true);
        table.center();

        Label title = new Label("GAME OVER", game.getSkin());
        title.setColor(Color.RED);
        title.setFontScale(2.5f);

        TextButton playButton = new TextButton("JUGAR DE NUEVO", game.getSkin());
        playButton.setColor(Color.RED);
        playButton.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                dispose();
                game.setScreen(new GameScreen(game, 1, 3, 0));
            }
        });
        TextButton mainMenuButton = new TextButton("MENU PRINCIPAL", game.getSkin());
        mainMenuButton.setColor(Color.RED);
        mainMenuButton.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                dispose();
                game.setScreen(new MainMenuScreen(game));
            }
        });
        TextButton exitButton = new TextButton("SALIR", game.getSkin());
        exitButton.setColor(Color.RED);
        exitButton.addListener(new ClickListener() {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                dispose();
                System.exit(0);
            }
        });

        Label aboutLabel = new Label("JUNGLE LOVE", game.getSkin());
        aboutLabel.setFontScale(2f);

        table.row().height(100);
        table.add(title).center().pad(35f);
        table.row().height(50);
        table.add(playButton).center().width(250).pad(5f);
        table.row().height(50);
        table.add(mainMenuButton).center().width(250).pad(5f);
        table.row().height(50);
        table.add(exitButton).center().width(250).pad(5f);
        table.row().height(50);
        table.add(aboutLabel).center().pad(55f);

        stage.addActor(table);
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act();
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

}
