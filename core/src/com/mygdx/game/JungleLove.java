package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.mygdx.game.screens.SplashScreen;

public class JungleLove extends Game {

	public SpriteBatch spriteBatch;
	public BitmapFont font;
	Skin skin;

	@Override
	public void create() {
		spriteBatch = new SpriteBatch();
		font = new BitmapFont();


		setScreen(new SplashScreen(this));
	}

	@Override
	public void render() {

		super.render();
	}

	@Override
	public void dispose() {
		spriteBatch.dispose();
		font.dispose();
	}

	public Skin getSkin() {
		if (skin == null)
			skin = new Skin(Gdx.files.internal("ui/uiskin.json"));

		return skin;
	}
}
