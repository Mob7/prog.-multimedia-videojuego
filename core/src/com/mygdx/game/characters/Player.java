package com.mygdx.game.characters;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.managers.ConfigurationManager;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.managers.SpriteManager;

import static com.mygdx.game.util.Constants.*;

/**
 * Created by sergio on 05/06/2016.
 */
public class Player extends Character {

    Animation rightAnimation, leftAnimation, deadRightAnimation, deadLeftAnimation, jumpRightAnimation, jumpLeftAnimation;
    public enum State {
        IDLE_RIGHT, IDLE_LEFT, RIGHT, LEFT, DEADRIGHT, DEADLEFT, JUMPRIGHT, JUMPLEFT
    }
    public State state;
    public int score;
    public int lives=3;
    private boolean faceRight;


    public Player(float x, float y, int lives, Vector2 velocity) {
        super(x, y, lives, velocity);

        rightAnimation = new Animation(0.15f, ResourceManager.getRegions("player_right"));
        leftAnimation = new Animation(0.15f, ResourceManager.getRegions("player_left"));
        jumpRightAnimation = new Animation(0.15f, ResourceManager.getRegions("player_jumping_right"));
        jumpLeftAnimation = new Animation(0.15f, ResourceManager.getRegions("player_jumping_left"));
        deadRightAnimation = new Animation(2f, ResourceManager.getRegions("death"));
        deadLeftAnimation = new Animation(2f, ResourceManager.getRegions("death"));

        state = State.IDLE_RIGHT;
        currentFrame = ResourceManager.getRegion("player_stopped_right");

        rect.width = PLAYER_WIDTH;
        rect.height = PLAYER_HEIGHT;

        faceRight = true;
    }

    @Override
    public void update(float dt, SpriteManager spriteManager) {
        if(score==100){
            lives++;
            score=0;
            if (ConfigurationManager.isSoundEnabled()) {
                ResourceManager.getSound(SOUND + "live.mp3").play();
            }
        }

        stateTime += dt;

        switch(state) {
            case IDLE_RIGHT:
                currentFrame = ResourceManager.getRegion("player_stopped_right");
                faceRight = true;
                break;
            case IDLE_LEFT:
                currentFrame = ResourceManager.getRegion("player_stopped_left");
                faceRight = false;
                break;
            case RIGHT:
                if (!isJumping)
                    currentFrame = rightAnimation.getKeyFrame(stateTime, true);
                else
                    currentFrame = ResourceManager.getRegion("player_right");
                faceRight = true;
                break;
            case LEFT:
                if (!isJumping)
                    currentFrame = leftAnimation.getKeyFrame(stateTime, true);
                else
                    currentFrame = ResourceManager.getRegion("player_left");
                faceRight = false;
                break;
            case JUMPRIGHT:
                currentFrame = jumpRightAnimation.getKeyFrame(stateTime, true);
                faceRight=true;
                break;
            case JUMPLEFT:
                currentFrame = jumpLeftAnimation.getKeyFrame(stateTime, true);
                faceRight = false;
                break;
            case DEADRIGHT:
                currentFrame = deadRightAnimation.getKeyFrame(stateTime, true);
                faceRight=true;
                break;
            case DEADLEFT:
                currentFrame = deadLeftAnimation.getKeyFrame(stateTime, true);
                faceRight=false;
                break;
            default:
                if(faceRight=true){
                    currentFrame = ResourceManager.getRegion("player_stopped_right");
                } else {
                    currentFrame = ResourceManager.getRegion("player_stopped_left");
                }
                break;
        }

        velocity.y -= GRAVITY * dt;
        velocity.scl(dt);
        rect.set(position.x, position.y, rect.getWidth(), rect.getHeight());
        if (!isDead())
            checkCollisions(spriteManager);
        velocity.scl(1 / dt);
        position.add(velocity);

        if (position.x < 0)
            position.x = 0;
        if ((position.y < 0) && (!isDead()))
            die();
    }

    @Override
    public void update(float dt) {

    }

    @Override
    public void render(SpriteBatch batch) {
        super.render(batch);
    }

    public void jump() {
        velocity.y = JUMPING_SPEED;
        isJumping = true;
        if (faceRight){
            state = State.JUMPRIGHT;
        } else {
            state = State.JUMPLEFT;
        }
        if (ConfigurationManager.isSoundEnabled()){
            ResourceManager.getSound(SOUND + "jump.mp3").play();
        }
    }

    @Override
    public void die() {

        if (!isDead()) {
            lives--;
            dead = true;
            if(faceRight){
                state = State.DEADRIGHT;
            } else {
                state = State.DEADLEFT;
            }
            if (ConfigurationManager.isSoundEnabled()) {
                ResourceManager.getSound(SOUND + "player_die.mp3").play(0.5f);
            }
            score = 0;
        }
    }

    @Override
    public void resurrect() {
        dead = false;
        state = State.IDLE_RIGHT;
        velocity.y = 0;
    }

    @Override
    public void checkCollisions(SpriteManager spriteManager) {
        if (!isDead())
            super.checkCollisions(spriteManager);
    }

    public int getLives(){
        return lives;
    }

    public void setLives(int lives){
        this.lives = lives;
    }

    public void setScore(int score){
        this.score = score;
    }
}
