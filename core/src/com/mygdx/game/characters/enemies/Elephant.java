package com.mygdx.game.characters.enemies;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.managers.SpriteManager;

import static com.mygdx.game.util.Constants.*;

/**
 * Created by sergio on 05/06/2016.
 */
public class Elephant extends Enemy {

    private boolean getUp;
    private long lastUp;
    private final static long GETTINGUP_TIME = 2000;

    public Elephant(float x, float y, int lives, Vector2 velocity) {
        super(x, y, lives, velocity);

        rightAnimation = new Animation(0.15f, ResourceManager.getRegions("elephant"));
        leftAnimation = new Animation(0.15f, ResourceManager.getRegions("elephant"));
        faceLeft = true;
        currentFrame = ResourceManager.getRegion("elephant",0);
        lastUp=0;

        rect.width = 71;
        rect.height = 38;
    }

    public void gettingUp(){

    }

    @Override
    public void update(float dt, SpriteManager spriteManager){
        super.update(dt, spriteManager);

        /*if (((TimeUtils.millis() - lastUp) >= GETTINGUP_TIME) && (!getUp)){
            int random = MathUtils.random(0, 10);
            if (random < 2){
                velocity.y = JUMPING_SPEED;
                lastUp = TimeUtils.millis();
                getUp = true;
            } else {
                lastUp = TimeUtils.millis();
            }
        }

        if (velocity.y < 0){
            getUp=false;
        }*/
    }
}
