package com.mygdx.game.characters.enemies;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.managers.ConfigurationManager;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.managers.SpriteManager;

import static com.mygdx.game.util.Constants.PLAYER_HEIGHT;
import static com.mygdx.game.util.Constants.SOUND;

/**
 * Created by sergio on 08/06/2016.
 */
public class Stone extends Enemy {
    boolean touchGround=false;
    boolean touchTop=false;
    float lastPos;

    public Stone(float x, float y, int lives, Vector2 velocity) {
        super(x, y, lives, velocity);


        rightAnimation = new Animation(0.05f, ResourceManager.getRegions("big_stone"));
        leftAnimation = new Animation(0.05f, ResourceManager.getRegions("big_stone"));
        faceLeft = true;
        currentFrame = ResourceManager.getRegion("big_stone", 0);


        rect.width = 50;
        rect.height = 50;
    }

    @Override
    public void update(float dt, SpriteManager spriteManager) {
        super.update(dt, spriteManager);
        if (position.y < 10) {
            touchGround = true;
            touchTop =  false;
            if (ConfigurationManager.isSoundEnabled()) {
                ResourceManager.getSound(SOUND + "shot.mp3").play();
            }
        }
        if(position.y > 459){
            touchTop = true;
            touchGround =  false;
            if (ConfigurationManager.isSoundEnabled()) {
                ResourceManager.getSound(SOUND + "shot.mp3").play();
            }
        }
        if(position.y == lastPos){
            touchGround = true;
            touchTop =  false;
            if (ConfigurationManager.isSoundEnabled()) {
                ResourceManager.getSound(SOUND + "shot.mp3").play();
            }
        }

        if (ConfigurationManager.isDificultyEnabled()) {
            if (touchGround) {
                velocity.y = 3;
            } else {
                velocity.y = -3;
                lastPos=position.y;
            }
            if (touchTop){
                velocity.y = -3;
            } else {
                velocity.y = 3;
                lastPos=position.y;
            }
        } else {
            if (touchGround) {
                velocity.y = 1;
            } else {
                velocity.y = -1;
                lastPos=position.y;
            }
            if (touchTop){
                velocity.y = -1;
            } else {
                velocity.y = 1;
                lastPos=position.y;
            }
        }
    }
}