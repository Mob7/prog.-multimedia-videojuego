package com.mygdx.game.characters.enemies;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.characters.Character;
import com.mygdx.game.managers.SpriteManager;
import static com.mygdx.game.util.Constants.*;


/**
 * Created by sergio on 05/06/2016.
 */
public abstract class Enemy extends Character{

    public enum EnemyType {
        stone, elephant, leon
    }
    protected EnemyType type;
    Animation rightAnimation, leftAnimation;
    public boolean faceLeft;

    public Enemy(float x, float y, int lives, Vector2 velocity) {
        super(x, y, lives, velocity);

        faceLeft = true;
    }

    @Override
    public void update(float dt, SpriteManager spriteManager) {

        stateTime += dt;

        if (faceLeft)
            currentFrame = leftAnimation.getKeyFrame(stateTime, true);
        else
            currentFrame = rightAnimation.getKeyFrame(stateTime, true);

        velocity.y -= GRAVITY * dt;
        velocity.scl(dt);
        rect.set(position.x, position.y, rect.getWidth(), rect.getHeight());
        checkCollisions(spriteManager);
        velocity.scl(1 / dt);
        position.add(velocity);
    }

    @Override
    public void update(float dt) {
    }

    @Override
    public void die() {
        dead = true;
    }

    @Override
    public void resurrect() {
        dead = false;
    }
}
