package com.mygdx.game.characters.enemies;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.TimeUtils;
import com.mygdx.game.managers.ResourceManager;
import com.mygdx.game.managers.SpriteManager;
import static com.mygdx.game.util.Constants.JUMPING_SPEED;

/**
 * Created by sergio on 05/06/2016.
 */
public class Leon extends Enemy {

    public Leon(float x, float y, int lives, Vector2 velocity) {
        super(x, y, lives, velocity);

        rightAnimation = new Animation(0.20f, ResourceManager.getRegions("leon"));
        leftAnimation =  new Animation(0.20f, ResourceManager.getRegions("leon"));
        faceLeft=true;
        currentFrame = ResourceManager.getRegion("leon",1);

        rect.width = 74;
        rect.height = 31;
    }


    @Override
    public void update(float dt, SpriteManager spriteManager){
        super.update(dt, spriteManager);

    }
}
