package com.mygdx.game.characters.animations;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.mygdx.game.characters.SpriteAnimation;
import com.mygdx.game.managers.ResourceManager;

/**
 * Created by sergio on 06/06/2016.
 */
public class Explosion extends SpriteAnimation {


    private Animation animation;

    public Explosion(float x, float y, String name){
        super(x,y);
        animation = new Animation(0.15f, ResourceManager.getRegions(name));
    }

    @Override
    public void update(float dt){
        stateTime += dt;
        currentFrame = animation.getKeyFrame(stateTime);
        if (animation.isAnimationFinished(stateTime)){
            die();
        }
    }

    @Override
    public void die(){
        dead = true;
    }

    @Override
    public void resurrect(){
        dead = false;
    }
}
