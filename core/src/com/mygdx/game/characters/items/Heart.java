package com.mygdx.game.characters.items;

import com.mygdx.game.managers.ResourceManager;

/**
 * Created by sergio on 05/06/2016.
 */
public class Heart extends Item {

    public Heart(float x, float y, int score) {
        super(x, y);
        this.score = score;

        currentFrame = ResourceManager.getRegion("live");
        rect.x = x;
        rect.y = y;
        rect.width = currentFrame.getRegionWidth();
        rect.height = currentFrame.getRegionHeight();
    }

    @Override
    public void update(float dt) {

    }
}
