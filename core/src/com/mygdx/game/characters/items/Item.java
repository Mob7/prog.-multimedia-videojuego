package com.mygdx.game.characters.items;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by sergio on 05/06/2016.
 */
public abstract class Item {

    public Vector2 position;
    TextureRegion currentFrame;
    public Rectangle rect;
    public int score;

    public Item(float x, float y) {

        position = new Vector2(x, y);
        rect = new Rectangle();
    }

    public void render(SpriteBatch batch) {
        batch.draw(currentFrame, position.x, position.y);
    }

    public abstract void update(float dt);
}
