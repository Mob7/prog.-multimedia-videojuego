package com.mygdx.game.characters.items;

import com.mygdx.game.managers.ResourceManager;

/**
 * Created by sergio on 06/06/2016.
 */
public class Goal extends Item {

    public Goal(float x, float y) {
        super(x, y);

        currentFrame = ResourceManager.getRegion("door2");
        rect.x = x;
        rect.y = y;
        rect.width = currentFrame.getRegionWidth();
        rect.height = currentFrame.getRegionHeight();
    }

    @Override
    public void update(float dt) {

    }
}
