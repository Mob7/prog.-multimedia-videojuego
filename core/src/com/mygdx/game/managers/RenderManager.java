package com.mygdx.game.managers;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.characters.SpriteAnimation;
import com.mygdx.game.characters.enemies.Enemy;
import com.mygdx.game.characters.items.Item;
import static com.mygdx.game.util.Constants.*;


/**
 * Created by sergio on 05/06/2016.
 */
public class RenderManager {


    SpriteBatch batch;
    BitmapFont font;
    SpriteManager spriteManager;
    CameraManager cameraManager;
    int level;
    int vidas;
    int score;

    public RenderManager(SpriteManager spriteManager, CameraManager cameraManager, SpriteBatch batch, int level, int vidas, int score) {
        this.spriteManager = spriteManager;
        this.cameraManager = cameraManager;
        this.batch = batch;
        this.level = level;
        this.vidas = vidas;
        this.score = score;
        font = new BitmapFont();
        spriteManager.player.setLives(vidas);
        spriteManager.player.setScore(score);
    }

    public void drawFrame() {
        batch.begin();
        drawHud();
        spriteManager.player.render(batch);
        for (Enemy enemy : spriteManager.enemies)
            enemy.render(batch);
        for (Item item : spriteManager.items)
            item.render(batch);
        for (SpriteAnimation animation : spriteManager.animations)
            animation.render(batch);
        batch.end();
    }

    private void drawHud() {

        batch.draw(ResourceManager.getRegion("hearthicon"), cameraManager.camera.position.x - CAMERA_WIDTH / 2 + 10, CAMERA_HEIGHT - TILE_HEIGHT);
        font.draw(batch, " x " + spriteManager.player.lives, cameraManager.camera.position.x - CAMERA_WIDTH / 2 + PLAYER_WIDTH + 10, CAMERA_HEIGHT - TILE_HEIGHT / 2);

        batch.draw(ResourceManager.getRegion("mushroomsIcon"), cameraManager.camera.position.x - CAMERA_WIDTH / 2 + 10 + TILE_WIDTH * 2, CAMERA_HEIGHT - PLAYER_HEIGHT);
        font.draw(batch, " x " + spriteManager.player.score, cameraManager.camera.position.x - CAMERA_WIDTH / 2 + PLAYER_WIDTH * 4 + 10, CAMERA_HEIGHT - TILE_HEIGHT / 2);

        batch.draw(ResourceManager.getRegion("level"), cameraManager.camera.position.x - CAMERA_WIDTH / 2 + 10 + TILE_WIDTH * 4, CAMERA_HEIGHT - PLAYER_HEIGHT);
        font.draw(batch, String.valueOf(level), cameraManager.camera.position.x - CAMERA_WIDTH / 2 + PLAYER_WIDTH * 8 + 10, CAMERA_HEIGHT - TILE_HEIGHT / 2);
    }

}
