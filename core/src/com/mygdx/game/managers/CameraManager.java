package com.mygdx.game.managers;

import com.badlogic.gdx.graphics.OrthographicCamera;
import static com.mygdx.game.util.Constants.*;

/**
 * Created by sergio on 05/06/2016.
 */
public class CameraManager {

    SpriteManager spriteManager;
    LevelManager levelManager;
    OrthographicCamera camera;

    public CameraManager(SpriteManager spriteManager, LevelManager levelManager) {
        this.spriteManager = spriteManager;
        this.levelManager = levelManager;

        init();
    }

    public void init() {
        camera = new OrthographicCamera();
        camera.setToOrtho(false, TILES_IN_CAMERA * TILE_WIDTH, TILES_IN_CAMERA * TILE_HEIGHT);
        camera.update();
    }

    public void handleCamera() {
        if (spriteManager.player.position.x < TILES_IN_CAMERA * TILE_WIDTH / 2)
            camera.position.set(TILES_IN_CAMERA * TILE_WIDTH / 2 , TILES_IN_CAMERA * TILE_HEIGHT / 2, 0);
        else
            camera.position.set(spriteManager.player.position.x, TILES_IN_CAMERA * TILE_HEIGHT / 2, 0);
        camera.update();
        levelManager.mapRenderer.setView(camera);
        levelManager.mapRenderer.render(new int[]{0, 1});
    }
}
