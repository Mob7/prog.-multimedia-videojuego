package com.mygdx.game.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.mygdx.game.JungleLove;
import com.mygdx.game.characters.Player;
import com.mygdx.game.characters.SpriteAnimation;
import com.mygdx.game.characters.enemies.Elephant;
import com.mygdx.game.characters.enemies.Enemy;
import com.mygdx.game.characters.enemies.Leon;
import com.mygdx.game.characters.enemies.Stone;
import com.mygdx.game.characters.items.Goal;
import com.mygdx.game.characters.items.Heart;
import com.mygdx.game.characters.items.Item;
import com.mygdx.game.characters.items.Mushroom;
import com.mygdx.game.screens.GameOverScreen;
import static com.mygdx.game.util.Constants.*;


import java.util.Iterator;

/**
 * Created by sergio on 05/06/2016.
 */
public class SpriteManager implements InputProcessor {

    JungleLove game;
    Player player;
    Array<Enemy> enemies;
    public Array<Item> items;
    Array<SpriteAnimation> animations;
    private Pool<Rectangle> rectPool = new Pool<Rectangle>() {
        @Override
        protected Rectangle newObject() {
            return new Rectangle();
        }
    };
    public Array<Rectangle> tiles;
    Music music;
    LevelManager levelManager;
    CameraManager cameraManager;
    Goal goal;

    public SpriteManager(JungleLove game) {
        this.game = game;

        enemies = new Array<Enemy>();
        items = new Array<Item>();
        animations = new Array<SpriteAnimation>();
        tiles = new Array<Rectangle>();

        Gdx.input.setInputProcessor(this);
    }

    public void setLevelManager(LevelManager levelManager) {
        this.levelManager = levelManager;
    }

    public void setCameraManager(CameraManager cameraManager) {
        this.cameraManager = cameraManager;
    }

    public void update(float dt) {

        if (!player.isDead())
            handleInput();

        updateCharacters(dt);
    }

    private void handleInput() {

        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            player.velocity.x = PLAYER_SPEED;
            player.state = Player.State.RIGHT;
        } else if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            player.velocity.x = -PLAYER_SPEED;
            player.state = Player.State.LEFT;
        } else {
            player.velocity.x = 0;
            if (player.state == Player.State.LEFT)
                player.state = Player.State.IDLE_LEFT;
            else if (player.state == Player.State.RIGHT)
                player.state = Player.State.IDLE_RIGHT;
        }
        if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
            if (!player.isJumping) {
                player.jump();
            }
            if (player.state == Player.State.LEFT) {
                player.state = Player.State.JUMPLEFT;
            } else if (player.state == Player.State.JUMPRIGHT) {
                player.state = Player.State.JUMPRIGHT;
            }
        }

    }

    private void updateCharacters(float dt) {
        updateEnemies(dt);
        updateItems(dt);
        updatePlayer(dt);
    }

    private void updateEnemies(float dt) {
        Enemy enemy;
        Iterator<Enemy> iterEnemies = enemies.iterator();
        while (iterEnemies.hasNext()) {
            enemy = iterEnemies.next();
            if (!cameraManager.camera.frustum.pointInFrustum(new Vector3(enemy.position.x, enemy.position.y, 0)) && (enemy.stateTime == 0))
                continue;
            enemy.update(dt, this);
            if (enemy.position.y <= 0) {
                iterEnemies.remove();
                continue;
            }
            if (enemy.position.x <= -enemy.rect.width) {
                iterEnemies.remove();
                continue;
            }
            if (!player.isDead()) {
                if (enemy.rect.overlaps(player.rect)) {
                    if (enemy instanceof Leon) {
                        if (ConfigurationManager.isSoundEnabled()) {
                            music.stop();
                        }
                        player.die();
                    }
                    if (enemy instanceof Elephant) {
                        if (ConfigurationManager.isSoundEnabled()) {
                            music.stop();
                        }
                        player.die();
                    }
                    if (enemy instanceof Stone) {
                        if (ConfigurationManager.isSoundEnabled()) {
                            music.stop();
                        }
                        player.die();
                    }
                }
            }
        }
    }

    private void updateItems(float dt) {

        Item item;
        Iterator<Item> iterItems = items.iterator();
        while (iterItems.hasNext()) {
            item = iterItems.next();
            if (item instanceof Mushroom) {
                if (item.rect.overlaps(player.rect)) {
                    player.score += item.score;
                    iterItems.remove();
                    if (ConfigurationManager.isSoundEnabled())
                        ResourceManager.getSound(SOUND + "blop.mp3").play();
                }
            } else if (item instanceof Heart) {
                if (item.rect.overlaps(player.rect)) {
                    player.lives += item.score;
                    iterItems.remove();
                    if (ConfigurationManager.isSoundEnabled())
                        ResourceManager.getSound(SOUND + "live.mp3").play();
                }
            } else if (item instanceof Goal) {
                if(item.rect.overlaps(player.rect)) {
                    item.update(dt);
                    iterItems.remove();
                }
            }
        }
    }

    private void updatePlayer(float dt) {
        player.update(dt, this);

        goal = levelManager.getGoal();
        if (goal != null) {
            if (player.rect.overlaps(goal.rect)) {
               levelManager.getNextLevel(player.getLives());

            }
        }
        if ((player.isDead()) && (player.position.y < -PLAYER_HEIGHT))
            if (player.lives > 0) {
                levelManager.restartCurrentLevel();
            } else {
                game.setScreen(new GameOverScreen(game));
                if (ConfigurationManager.isSoundEnabled()) {
                    ResourceManager.getSound(SOUND + "game_over.wav").play();
                }
            }
    }

    public void getCollisionTiles(com.mygdx.game.characters.Character character, int startX, int endX, int startY, int endY) {

        tiles.clear();
        int xCell, yCell;
        for (int y = startY; y <= endY; y++) {
            for (int x = startX; x <= endX; x++) {
                xCell = x / TILE_WIDTH;
                yCell = y / TILE_HEIGHT;
                TiledMapTileLayer.Cell cell = levelManager.collisionLayer.getCell(xCell, yCell);
                if ((cell != null) && (cell.getTile().getProperties().containsKey("blocked"))) {
                    Rectangle rect = rectPool.obtain();
                    if (character.velocity.y > 0) {
                        if (!(cell.getTile().getProperties().get("blocked").equals("true")))
                            rect.set(x, y, 1, 1);
                    } else
                        rect.set((int) (Math.ceil(x / TILE_WIDTH) * TILE_WIDTH), (int) (Math.ceil(y / TILE_HEIGHT) * TILE_HEIGHT), TILE_WIDTH, TILE_WIDTH);

                    tiles.add(rect);
                }
            }
        }
    }

        @Override
        public boolean keyDown (int keycode){
            return false;
        }

        @Override
        public boolean keyUp (int keycode){
            return false;
        }

        @Override
        public boolean keyTyped (char character){
            return false;
        }

        @Override
        public boolean touchDown (int screenX, int screenY, int pointer, int button){
            return false;
        }

        @Override
        public boolean touchUp (int screenX, int screenY, int pointer, int button){
            return false;
        }

        @Override
        public boolean touchDragged (int screenX, int screenY, int pointer){
            return false;
        }

        @Override
        public boolean mouseMoved (int screenX, int screenY){
            return false;
        }

        @Override
        public boolean scrolled (int amount){
            return false;
        }
    }
