package com.mygdx.game.managers;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.objects.TiledMapTileMapObject;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.mygdx.game.characters.Player;
import com.mygdx.game.characters.enemies.Stone;
import com.mygdx.game.characters.enemies.Elephant;
import com.mygdx.game.characters.enemies.Enemy;
import com.mygdx.game.characters.enemies.Leon;
import com.mygdx.game.characters.items.Goal;
import com.mygdx.game.characters.items.Heart;
import com.mygdx.game.characters.items.Item;
import com.mygdx.game.characters.items.Mushroom;
import com.mygdx.game.screens.EndGameScreen;
import com.mygdx.game.screens.GameScreen;

import static com.mygdx.game.util.Constants.*;
import static sun.audio.AudioPlayer.player;

/**
 * Created by sergio on 05/06/2016.
 */
public class LevelManager {
    public static int currentLevel = 1;
    public static Array<Enemy> enemies = new Array<Enemy>();
    public static Array<Item> items = new Array<Item>();

    TiledMap map;
    TiledMapTileLayer collisionLayer;
    MapLayer objectLayer;
    OrthogonalTiledMapRenderer mapRenderer;
    SpriteManager spriteManager;
    CameraManager cameraManager;
    public SpriteBatch batch;
    Goal goal;
    int level;
    int lives;

    public LevelManager(SpriteManager spriteManager) {
        this.spriteManager = spriteManager;
        level = 1;
    }

    public void setCameraManager(CameraManager cameraManager) {
        this.cameraManager = cameraManager;
    }

    public void loadCurrentLevel() {

        if(level==2){
            spriteManager.enemies.clear();
            spriteManager.items.clear();
            spriteManager.animations.clear();
            spriteManager.tiles.clear();
            map = new TmxMapLoader().load("levels/level2.tmx");
        } else {
            map = new TmxMapLoader().load("levels/level1.tmx");
        }
            collisionLayer = (TiledMapTileLayer) map.getLayers().get("terrain");
            objectLayer = map.getLayers().get("objects");
            mapRenderer = new OrthogonalTiledMapRenderer(map);
            batch = (SpriteBatch)mapRenderer.getBatch();


        spriteManager.player = new Player(0, 0, 3, new Vector2(0, 0));
            spriteManager.player.position.set(0 * TILE_WIDTH, 10 * TILE_HEIGHT);

            loadEnemies();
            loadItems();
            playCurrentLevelMusic();


    }

    public void restartCurrentLevel() {
        if (ConfigurationManager.isSoundEnabled()) {
            spriteManager.music.stop();
            cameraManager.init();

            spriteManager.enemies.clear();
            spriteManager.items.clear();
            spriteManager.animations.clear();
            spriteManager.tiles.clear();

            spriteManager.player.position.set(0 * TILE_WIDTH, 10 * TILE_HEIGHT);
            spriteManager.player.resurrect();
            loadEnemies();
            loadItems();
            playCurrentLevelMusic();
            spriteManager.player.setScore(0);
        } else {
            cameraManager.init();

            spriteManager.enemies.clear();
            spriteManager.items.clear();
            spriteManager.animations.clear();
            spriteManager.tiles.clear();

            spriteManager.player.position.set(0 * TILE_WIDTH, 10 * TILE_HEIGHT);
            spriteManager.player.resurrect();
            loadEnemies();
            loadItems();
            spriteManager.player.setScore(0);
        }

    }

    public void getNextLevel(int lives) {
        this.lives = lives;
        level++;
        if(getLevel()==3){
            spriteManager.game.setScreen(new EndGameScreen(spriteManager.game));
            if (ConfigurationManager.isSoundEnabled()) {
                ResourceManager.getSound(SOUND + "game_ended.mp3").play();
                spriteManager.music.stop();
            }

        } else {
            if (ConfigurationManager.isSoundEnabled()) {
                spriteManager.music.stop();

                try {
                    ResourceManager.getSound(SOUND + "levelwin.mp3").play();
                    Thread.sleep(2000);
                } catch (InterruptedException ie) {}
                setLevel(2);
                spriteManager.game.setScreen(new GameScreen(spriteManager.game, 2, getLives(), spriteManager.player.score));
                loadCurrentLevel();
            } else {
                setLevel(2);
                spriteManager.game.setScreen(new GameScreen(spriteManager.game, 2, getLives(), spriteManager.player.score));
                loadCurrentLevel();
            }
        }


    }

    private void playCurrentLevelMusic() {
        if (ConfigurationManager.isSoundEnabled()) {
            spriteManager.music = ResourceManager.getMusic(MUSIC + "bso.mp3");
            spriteManager.music.setLooping(true);
            spriteManager.music.setVolume(.2f);
            spriteManager.music.play();

            ResourceManager.getSound(SOUND + "game_begin.mp3").play();
        }
    }

    public void loadEnemies() {

        Enemy enemy = null;
        for (MapObject object : map.getLayers().get("objects").getObjects()) {
            if (object instanceof TiledMapTileMapObject) {
                TiledMapTileMapObject rectangleObject = (TiledMapTileMapObject) object;
                if (rectangleObject.getProperties().containsKey("enemy")) {
                    String enemyType = (String) rectangleObject.getProperties().get("enemy");
                    if(enemyType.equals("lion")) {
                        Rectangle rectLion = new Rectangle(rectangleObject.getX(), rectangleObject.getY(), rectangleObject.getTextureRegion().getRegionWidth(),
                                rectangleObject.getTextureRegion().getRegionHeight());
                        if (ConfigurationManager.isDificultyEnabled()){
                            enemy = new Leon(rectLion.x, rectLion.y, 1, new Vector2(-ENEMY_SPEED+5f, 0));
                        } else {
                            enemy = new Leon(rectLion.x, rectLion.y, 1, new Vector2(-ENEMY_SPEED, 0));
                        }
                        LevelManager.enemies.add(enemy);
                    } else if(enemyType.equals("elephant")) {
                        Rectangle rectEleph = new Rectangle(rectangleObject.getX(), rectangleObject.getY(), rectangleObject.getTextureRegion().getRegionWidth(),
                                rectangleObject.getTextureRegion().getRegionHeight());
                        enemy = new Elephant(rectEleph.x, rectEleph.y, 1, new Vector2(0, 0));
                        LevelManager.enemies.add(enemy);
                    } else if(enemyType.equals("stone")){
                            Rectangle rectStone = new Rectangle(rectangleObject.getX(), rectangleObject.getY(),rectangleObject.getTextureRegion().getRegionWidth(),
                                    rectangleObject.getTextureRegion().getRegionHeight());
                            enemy = new Stone (rectStone.x, rectStone.y, 1, new Vector2(0, 0));
                        LevelManager.enemies.add(enemy);
                    }
                    spriteManager.enemies.add(enemy);
                }
            }
        }
    }

    private void loadItems() {

        Item item = null;
        for (MapObject object : map.getLayers().get("objects").getObjects()) {
            if (object instanceof TiledMapTileMapObject) {
                TiledMapTileMapObject rectangleObject = (TiledMapTileMapObject) object;
                if (rectangleObject.getProperties().containsKey("item")) {
                    String itemType = (String) rectangleObject.getProperties().get("item");
                    if (itemType.equals("mushroom")) {
                        int score = Integer.parseInt((String) rectangleObject.getProperties().get("score"));
                        Rectangle rectMushroom = new Rectangle(rectangleObject.getX(), rectangleObject.getY(), rectangleObject.getTextureRegion().getRegionWidth(),
                                rectangleObject.getTextureRegion().getRegionHeight());
                        item = new Mushroom(rectMushroom.x, rectMushroom.y, score);
                        spriteManager.items.add(item);
                        LevelManager.items.add(item);
                    } else if (itemType.equals("heart")) {
                        int live = Integer.parseInt((String) rectangleObject.getProperties().get("live"));
                        Rectangle rectHeart = new Rectangle(rectangleObject.getX(), rectangleObject.getY(), rectangleObject.getTextureRegion().getRegionWidth(),
                                rectangleObject.getTextureRegion().getRegionHeight());
                        item = new Heart(rectHeart.x, rectHeart.y, live);
                        spriteManager.items.add(item);
                        LevelManager.items.add(item);
                    } else if (itemType.equals("goal")) {
                        Rectangle rectGoal = new Rectangle(rectangleObject.getX(), rectangleObject.getY(), rectangleObject.getTextureRegion().getRegionWidth(),
                                rectangleObject.getTextureRegion().getRegionHeight());
                        item = new Goal(rectGoal.x, rectGoal.y);
                        goal = new Goal(rectGoal.x, rectGoal.y);
                        spriteManager.items.add(item);
                        LevelManager.items.add(item);
                    }
                }
            }
        }
    }

    public int getMushroomCount() {
        int count = 0;
        for (Item item : spriteManager.items) {
            if (item instanceof Mushroom)
                count++;
        }
        return count;
    }

    public int getLevel(){
        return level;
    }

    public void setLevel(int level){
        this.level = level;
    }

    public Goal getGoal(){
        return goal;
    }

    public TiledMap getMap(){
        return map;
    }

    public void setMap(TiledMap map){
        this.map = map;
    }

    public int getLives(){
        return lives;
    }
}
