package com.mygdx.game.managers;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import static com.mygdx.game.util.Constants.*;
import java.io.File;

/**
 * Created by sergio on 05/06/2016.
 */
public class ResourceManager {

    public static AssetManager assets = new AssetManager();

    public static void loadAllResources() {
        assets.load(TEXTURE_ATLAS, TextureAtlas.class);

        loadSounds();
        loadMusics();
    }

    public static boolean update() {
        return assets.update();
    }

    public static void loadSounds() {

        assets.load("sounds" + File.separator + "game_begin.mp3", Sound.class);
        assets.load("sounds" + File.separator + "game_over.wav", Sound.class);
        assets.load("sounds" + File.separator + "jump.mp3", Sound.class);
        assets.load("sounds" + File.separator + "player_die.mp3", Sound.class);
        assets.load("sounds" + File.separator + "blop.mp3", Sound.class);
        assets.load("sounds" + File.separator + "live.mp3", Sound.class);
        assets.load("sounds" + File.separator + "game_ended.mp3", Sound.class);
        assets.load("sounds" + File.separator + "levelwin.mp3", Sound.class);
        assets.load("sounds" + File.separator + "shot.mp3", Sound.class);
    }

    public static void loadMusics() {
        assets.load("musics" + File.separator + "bso.mp3", Music.class);

    }

    public static TextureRegion getRegion(String name) {

        return assets.get(TEXTURE_ATLAS, TextureAtlas.class).findRegion(name);
    }

    public static TextureRegion getRegion(String name, int position) {

        return assets.get(TEXTURE_ATLAS, TextureAtlas.class).findRegion(name, position);
    }

    public static Array<TextureAtlas.AtlasRegion> getRegions(String name) {

        return assets.get(TEXTURE_ATLAS, TextureAtlas.class).findRegions(name);
    }

    public static Sound getSound(String name) {
        return assets.get(name, Sound.class);
    }

    public static Music getMusic(String name) {
        return assets.get(name, Music.class);
    }

}
