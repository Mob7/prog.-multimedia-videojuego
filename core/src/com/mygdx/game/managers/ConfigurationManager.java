package com.mygdx.game.managers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.mygdx.game.util.Constants;

/**
 * Created by sergio on 05/06/2016.
 */
public class ConfigurationManager {

    private static Preferences prefs = Gdx.app.getPreferences(Constants.APP_NAME);

    public static boolean isSoundEnabled() {
        return prefs.getBoolean("sound");
    }

    public static boolean isDificultyEnabled() {
        return prefs.getBoolean("dificulty");
    }
}
